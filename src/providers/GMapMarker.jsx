import { number, object, shape } from 'prop-types';
import { useEffect, useState } from 'react';
import { getGoogleMapsVersion } from './GMapWrapper';

function GMapMarker({ position, map, onClick }) {
  const [marker, setMarker] = useState(null);

  useEffect(() => {
    if (getGoogleMapsVersion()) {
      const newMarker = new window.google.maps.Marker();
      newMarker.addListener('click', onClick);
      setMarker(newMarker);

      return () => newMarker.setMap(null);
    }
  }, []);

  useEffect(() => {
    if (marker) marker.setOptions({ position, map });
  }, [marker, position, map]);

  return null;
}

GMapMarker.propTypes = {
  position: shape({
    lat: number,
    lng: number
  }),
  map: object
};

export default GMapMarker;
