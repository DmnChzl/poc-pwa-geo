import { useEffect } from 'react';
import { bool, node } from 'prop-types';

function WakeLockProvider({ disabled = false, children }) {
  useEffect(() => {
    let lockScreen;

    if ('wakeLock' in navigator && !disabled) {
      navigator.wakeLock.request('screen').then(lock => {
        lockScreen = lock;
      });
    }

    return () => {
      if (lockScreen) {
        wakeLock.release().then(() => {
          lockScreen = null;
        });
      }
    };
  }, []);

  return children;
}

WakeLockProvider.propTypes = {
  disabled: bool,
  children: node
};

export default WakeLockProvider;
