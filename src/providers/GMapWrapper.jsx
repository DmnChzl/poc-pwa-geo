import { node, string } from 'prop-types';
import { useEffect, useState } from 'react';

export const getGoogleMapsVersion = () => window.google && window.google.maps && window.google.maps.version;

function GMapWrapper({ apiKey, children }) {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    if (getGoogleMapsVersion()) {
      setLoaded(true);
      return;
    }

    const script = document.createElement('script');
    script.async = true;
    script.defer = true;
    script.onload = () => setLoaded(true);
    script.src = `https://maps.googleapis.com/maps/api/js${apiKey ? `?key=${apiKey}` : ''}`;
    document.head.appendChild(script);

    // return () => document.head.removeChild(script);
  }, []);

  return loaded ? children : null;
}

GMapWrapper.propTypes = {
  apiKey: string,
  children: node
};

export default GMapWrapper;
