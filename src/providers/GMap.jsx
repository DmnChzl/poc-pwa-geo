import { bool, func, number, shape } from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import { getGoogleMapsVersion } from './GMapWrapper';

function GMap({ defaultCenter = { lat: 0, lng: 0 }, defaultZoom = 10, enableUI = false, children = () => {} }) {
  const ref = useRef(null);
  const [map, setMap] = useState(null);

  useEffect(() => {
    if (getGoogleMapsVersion()) {
      const newMap = new window.google.maps.Map(ref.current, {
        center: {
          lat: defaultCenter.lat,
          lng: defaultCenter.lng
        },
        zoom: defaultZoom,
        disableDefaultUI: !enableUI
      });

      setMap(newMap);
    }
  }, []);

  return (
    <div ref={ref} style={{ height: '100%' }}>
      {children({ map })}
    </div>
  );
}

GMap.propTypes = {
  defaultCenter: shape({
    lat: number,
    lng: number
  }),
  defaultZoom: number,
  enableUI: bool,
  children: func
};

export default GMap;
