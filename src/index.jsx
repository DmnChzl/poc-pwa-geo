import '@fontsource/poppins';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './App';
import './index.css';
import SignalsProvider from './SignalsContext';
import WakeLockProvider from './providers/WakeLockProvider';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <WakeLockProvider>
    <Router>
      <SignalsProvider>
        <App />
      </SignalsProvider>
    </Router>
  </WakeLockProvider>
);
