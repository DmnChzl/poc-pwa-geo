export const ITEM_KEY = 'geo_pwned';

export const NIORT_COORDS = { lat: 46.3238, lng: -0.4654 };

export const THEME = {
  DARK: 'dark',
  LIGHT: 'light'
};

export const COLOR = {
  GRAY_800: '#1f2937',
  GRAY_100: '#f3f4f6'
};

export const CARDVIEW_TYPE = {
  SUCCESS: 'success',
  DANGER: 'danger',
  WARNING: 'warning',
  INFO: 'info',
  DEFAULT: 'default'
};
