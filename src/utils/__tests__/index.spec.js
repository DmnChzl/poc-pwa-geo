import { expect, it } from 'vitest';
import { formatTimestamp, isEmpty, randomString, reverseSortBy } from '../index';

it('formatTimestamp', () => {
  const thirtyOctober = new Date('2022-10-30');
  expect(formatTimestamp(thirtyOctober.getTime(), 'dd/MM/yyyy')).toEqual('30/10/2022');
  expect(formatTimestamp('dd/MM/yyyy')).toHaveLength(0);
});

it('isEmpty', () => {
  expect(isEmpty({ value: 42 })).toBeFalsy();
  expect(isEmpty({})).toBeTruthy();
});

it('randomString', () => {
  const randomStr = randomString();
  expect(randomStr.length).toBeGreaterThanOrEqual(10);
  expect(randomStr).not.toEqual(randomString());
});

it('reverseSortBy', () => {
  const arr = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89].map(val => ({ str: val + '', num: val }));
  const [eightyNine, ...sortByNum] = arr.sort(reverseSortBy('num'));
  expect([eightyNine, ...sortByNum]).toHaveLength(10);
  expect(eightyNine['str']).toEqual('89');
});
