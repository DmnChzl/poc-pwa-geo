import { format } from 'date-fns';

export const downloadFile = file => {
  const link = document.createElement('a');
  const url = URL.createObjectURL(file);

  link.href = url;
  link.download = file.name;

  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);

  URL.revokeObjectURL(url);
};

export const formatTimestamp = (timestamp, pattern) => {
  try {
    return format(new Date(timestamp), pattern);
  } catch {
    return '';
  }
};

export const isEmpty = obj => {
  try {
    return Object.entries(obj).length === 0;
  } catch {
    throw new Error('NaO: Not an Object');
  }
};

export const randomString = () => Math.random().toString(36).substring(2);

export const reverseSortBy = key => (a, b) => a[key] > b[key] ? -1 : a[key] < b[key] ? 1 : 0;
