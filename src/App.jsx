import { useEffect, useState } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import { ITEM_KEY, NIORT_COORDS } from './constants';
import useInterval from './hooks/useInterval';
import GMap from './providers/GMap';
import GMapMarker from './providers/GMapMarker';
import GMapWrapper from './providers/GMapWrapper';
import * as LocationService from './services/locationService';
import * as StorageService from './services/storageService';
import { useSignalsContext } from './SignalsContext';
import { randomString } from './utils';
import HistoryView from './views/HistoryView';
import Layout from './views/Layout';
import LiveView from './views/LiveView';

export default function App(props) {
  const [{ records, distinctRecords, isListening }, { addRecord, setRecords }] = useSignalsContext();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    StorageService.getItem(ITEM_KEY, true).then(item => {
      if (item?.records) setRecords(item.records);
    });
  }, []);

  useEffect(() => {
    StorageService.patchItem(ITEM_KEY, { records: records.value }, true);
  }, [records.value]);

  const triggerCurrentPosition = async () => {
    setLoading(true);

    const record = {
      uid: randomString(),
      timestamp: new Date().getTime()
    };

    try {
      const { coords } = await LocationService.getCurrentPosition();

      addRecord({
        ...record,
        latitude: coords.latitude,
        longitude: coords.longitude
      });

      if (error) setError('');
    } catch (err) {
      setError(err.message);
    } finally {
      setLoading(false);
    }
  };

  useInterval(() => {
    if (isListening.value) triggerCurrentPosition();
  }, 30 * 1000);

  const getDistinctRecords = records => {
    return records.reduce((acc, record) => {
      const recordExists = acc.find(rec => rec.latitude === record.latitude && rec.longitude === record.longitude);
      return !recordExists ? [...acc, record] : acc;
    }, []);
  };

  return (
    <Routes>
      <Route
        path="/"
        element={<Layout getCurrentPosition={triggerCurrentPosition} loading={loading} isError={!!error} />}>
        <Route index element={<Navigate to="/live" />} />
        <Route
          path="live"
          element={<LiveView errorMessage={error} distinctRecordsLength={getDistinctRecords(records.value).length} />}
        />
        <Route path="history" element={<HistoryView />} />
        <Route
          path="map"
          element={
            <div className="flex-auto">
              <GMapWrapper apiKey={import.meta.env['VITE_API_KEY']}>
                <GMap defaultCenter={NIORT_COORDS} defaultZoom={10}>
                  {props =>
                    getDistinctRecords(records.value).map(({ latitude, longitude }, idx) => (
                      <GMapMarker
                        key={idx}
                        position={{ lat: latitude, lng: longitude }}
                        {...props}
                        onClick={() => console.log(idx, { lat: latitude, lng: longitude })}
                      />
                    ))
                  }
                </GMap>
              </GMapWrapper>
            </div>
          }
        />
        <Route path="*" element={<main className="flex-auto">No Match</main>} />
      </Route>
    </Routes>
  );
}
