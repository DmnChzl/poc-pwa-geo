import { describe, expect, it } from 'vitest';
import createListeningState from '../listeningState';

describe('listeningState', () => {
  const state = createListeningState();

  it('playOn', () => {
    const [{ isListening }, { playOn }] = state;

    playOn();
    expect(isListening.value).toBeTruthy();
  });

  it('playOff', () => {
    const [{ isListening }, { playOff }] = state;

    playOff();
    expect(isListening.value).toBeFalsy();
  });
});
