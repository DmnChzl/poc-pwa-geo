import { describe, expect, it } from 'vitest';
import createRecordsState from '../recordsState';

describe('recordsState', () => {
  const state = createRecordsState();

  it('recordsLength', () => {
    const [{ recordsLength }, { addRecord, resetRecords }] = state;

    resetRecords();
    addRecord({ uid: 'a1b2c3d4e5' });
    expect(recordsLength.value).toEqual(1);
  });

  it('lastRecord', () => {
    const [{ lastRecord }, { addRecord, resetRecords }] = state;

    resetRecords();
    addRecord({ uid: 'a1b2c3d4e5' });
    addRecord({ uid: 'b2c3d4e5f6' });
    addRecord({ uid: 'c3d4e5f6g7' });
    expect(lastRecord.value).toEqual({ uid: 'c3d4e5f6g7' });
  });
});
