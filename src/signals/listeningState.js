import { computed, effect, signal } from '@preact/signals-react';

export default function createListeningState() {
  const play = signal(true);

  const isListening = computed(() => !!play.value);

  const playOn = () => (play.value = true);
  const playOff = () => (play.value = false);

  const toggleListening = () => {
    if (isListening.value) {
      playOff();
    } else {
      playOn();
    }
  };

  effect(() => console.log('Signal<{ play }>', play.value));

  return [{ isListening }, { playOn, playOff, toggleListening }];
}
