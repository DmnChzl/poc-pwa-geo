import { computed, effect, signal } from '@preact/signals-react';

export default function createRecordsState() {
  const records = signal([]);

  const recordsLength = computed(() => records.value.length);

  const lastRecord = computed(() => records.value[recordsLength.value - 1]);

  const addRecord = newRecord => {
    records.value = [...records.value, newRecord];
  };

  const setRecords = newRecords => {
    records.value = newRecords;
  };

  const resetRecords = () => {
    records.value = [];
  };

  effect(() => console.log('Signal<{ records }>', records.value));

  return [
    { records, recordsLength, lastRecord },
    { addRecord, setRecords, resetRecords }
  ];
}
