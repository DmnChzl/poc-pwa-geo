import { node } from 'prop-types';
import { createContext, useContext } from 'react';
import createListeningState from './signals/listeningState';
import createRecordsState from './signals/recordsState';

const SignalsContext = createContext();

export default function SignalsProvider({ children }) {
  const [recordsGetters, recordsSetters] = createRecordsState();
  const [listeningGetters, listeningSetters] = createListeningState();

  return (
    <SignalsContext.Provider
      value={[
        { ...recordsGetters, ...listeningGetters },
        { ...recordsSetters, ...listeningSetters }
      ]}>
      {children}
    </SignalsContext.Provider>
  );
}

SignalsProvider.propTypes = {
  children: node
};

export const useSignalsContext = () => useContext(SignalsContext);
