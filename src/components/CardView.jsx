import clsx from 'clsx';
import { func, node, oneOf, string } from 'prop-types';
import { CARDVIEW_TYPE } from '../constants';

const getClassName = type => {
  switch (type) {
    case CARDVIEW_TYPE.SUCCESS:
      return 'text-green-200';
    case CARDVIEW_TYPE.DANGER:
      return 'text-red-200';
    case CARDVIEW_TYPE.WARNING:
      return 'text-yellow-200';
    case CARDVIEW_TYPE.INFO:
      return 'text-blue-200';
    case CARDVIEW_TYPE.DEFAULT:
      return 'text-gray-400';
    default:
      return '';
  }
};

function CardView({
  className,
  title = '',
  subTitle = '',
  type = CARDVIEW_TYPE.DEFAULT,
  renderAdornment = () => {},
  children,
  ...props
}) {
  return (
    <div
      className={clsx(
        'p-4 flex flex-col space-y-4 text-white dark:text-gray-200 transition-colors rounded shadow',
        type === CARDVIEW_TYPE.SUCCESS && 'bg-green-600',
        type === CARDVIEW_TYPE.DANGER && 'bg-red-600',
        type === CARDVIEW_TYPE.WARNING && 'bg-yellow-600',
        type === CARDVIEW_TYPE.INFO && 'bg-blue-600',
        type === CARDVIEW_TYPE.DEFAULT && 'bg-gray-800',
        className
      )}
      {...props}>
      <div className="flex justify-between">
        <div className="flex flex-col justify-center">
          {<span className="tracking-wide">{title || 'Unknown'}</span>}
          {subTitle && (
            <span
              className={clsx(
                'tracking-wide',
                type === CARDVIEW_TYPE.SUCCESS && 'text-green-200',
                type === CARDVIEW_TYPE.DANGER && 'text-red-200',
                type === CARDVIEW_TYPE.WARNING && 'text-yellow-200',
                type === CARDVIEW_TYPE.INFO && 'text-blue-200',
                type === CARDVIEW_TYPE.DEFAULT && 'text-gray-400'
              )}>
              {subTitle}
            </span>
          )}
        </div>
        {renderAdornment({ className: getClassName(type) })}
      </div>
      {children}
    </div>
  );
}

CardView.propTypes = {
  className: string,
  title: string,
  subTitle: string,
  type: oneOf([
    CARDVIEW_TYPE.SUCCESS,
    CARDVIEW_TYPE.DANGER,
    CARDVIEW_TYPE.WARNING,
    CARDVIEW_TYPE.INFO,
    CARDVIEW_TYPE.DEFAULT
  ]),
  renderAdornment: func,
  children: node
};

export default CardView;
