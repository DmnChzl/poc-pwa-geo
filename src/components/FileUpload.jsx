import clsx from 'clsx';
import { func, node, string } from 'prop-types';

function FileUpload({ className, id, onChange, children }) {
  return (
    <div className={clsx('relative', className)}>
      <input
        className="absolute w-0 h-0 opacity-0 overflow-hidden -z-10"
        id={id || 'file-upload'}
        type="file"
        onChange={onChange}
      />
      <label className="flex items-center cursor-pointer" htmlFor={id || 'file-upload'}>
        {children}
      </label>
    </div>
  );
}

FileUpload.propTypes = {
  className: string,
  id: string,
  onChange: func,
  children: node
};

export default FileUpload;
