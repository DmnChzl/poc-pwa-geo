import HeroArrowDownTray from './HeroArrowDownTray';
import HeroArrowPath from './HeroArrowPath';
import HeroArrowUpTray from './HeroArrowUpTray';
import HeroCrossMark from './HeroCrossMark';
import HeroEllipsisVertical from './HeroEllipsisVertical';
import HeroExclamationTriangle from './HeroExclamationTriangle';
import HeroMap from './HeroMap';
import HeroMapPin from './HeroMapPin';
import HeroMoon from './HeroMoon';
import HeroPause from './HeroPause';
import HeroPlay from './HeroPlay';
import HeroQueueList from './HeroQueueList';
import HeroSun from './HeroSun';
import HeroTrash from './HeroTrash';

export {
  HeroArrowDownTray,
  HeroArrowPath,
  HeroArrowUpTray,
  HeroCrossMark,
  HeroExclamationTriangle,
  HeroEllipsisVertical,
  HeroMap,
  HeroMapPin,
  HeroMoon,
  HeroPause,
  HeroPlay,
  HeroQueueList,
  HeroSun,
  HeroTrash
};
