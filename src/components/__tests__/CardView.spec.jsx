import { render, screen } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import CardView from '../CardView';

describe('<CardView />', () => {
  it('Should Renders', () => {
    render(
      <CardView title="Title" subTitle="SubTitle">
        Lorem Ipsum Dolor Sit Amet
      </CardView>
    );

    expect(screen.getByText('Title')).toBeInTheDocument();
    expect(screen.getByText('SubTitle')).toBeInTheDocument();
    expect(screen.getByText('Lorem Ipsum Dolor Sit Amet')).toBeInTheDocument();
  });
});
