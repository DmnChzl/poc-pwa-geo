import { render, screen } from '@testing-library/react';
import { describe, expect, it } from 'vitest';
import FileUpload from '../FileUpload';

describe('<FileUpload />', () => {
  it('Should Renders', () => {
    render(<FileUpload>File Upload</FileUpload>);

    expect(screen.getByText('File Upload')).toBeInTheDocument();
  });
});
