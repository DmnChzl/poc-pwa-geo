import { func } from 'prop-types';
import { useNavigate } from 'react-router-dom';
import FileUpload from '../components/FileUpload';
import {
  HeroArrowDownTray,
  HeroArrowPath,
  HeroArrowUpTray,
  HeroCrossMark,
  HeroMapPin,
  HeroMoon,
  HeroPause,
  HeroPlay,
  HeroQueueList,
  HeroSun,
  HeroTrash
} from '../components/icons';
import { ITEM_KEY } from '../constants';
import * as StorageService from '../services/storageService';
import { useSignalsContext } from '../SignalsContext';
import { downloadFile } from '../utils';

function Menu({ onClose, toggleTheme, getCurrentPosition, ...props }) {
  const [{ isListening }, { setRecords, resetRecords, toggleListening }] = useSignalsContext();
  const navigate = useNavigate();

  const handleClose = callback => {
    return () => {
      callback();
      onClose();
    };
  };

  const goTo = pathname => navigate(pathname);

  const importData = event => {
    const [file] = event.target.files;
    const reader = new FileReader();

    reader.addEventListener('load', event => {
      const records = JSON.parse(event.target.result);
      if (Array.isArray(records)) setRecords(records);
    });

    reader.readAsText(file);
  };

  const exportFile = async () => {
    const item = await StorageService.getItem(ITEM_KEY, true);

    if (item?.records) {
      const file = new File([JSON.stringify(item.records)], 'records.json', {
        type: 'application/json'
      });

      downloadFile(file);
    }
  };

  return (
    <div className="absolute top-2 right-2 p-2 flex flex-col space-y-4 bg-white dark:bg-gray-800 text-gray-800 dark:text-gray-100 border border-gray-300 dark:border-gray-600 rounded shadow">
      <div className="flex justify-between">
        <span className="font-semibold tracking-wide">MENU</span>
        <button onClick={onClose}>
          <HeroCrossMark />
        </button>
      </div>

      <ul className="flex flex-col space-y-4">
        <li>
          <button
            className="w-full flex hover:text-blue-600 dark:hover:text-blue-400 transition-colors"
            type="button"
            onClick={handleClose(toggleListening)}>
            {isListening.value ? <HeroPause /> : <HeroPlay />}
            <span className="ml-2">{isListening.value ? 'Pause' : 'Restart Listening'}</span>
          </button>
        </li>

        <li>
          <button
            className="w-full flex hover:text-blue-600 dark:hover:text-blue-400 transition-colors"
            type="button"
            onClick={handleClose(getCurrentPosition)}>
            <HeroArrowPath />
            <span className="ml-2">Get Current Position</span>
          </button>
        </li>

        <li>
          <button
            className="w-full flex hover:text-blue-600 dark:hover:text-blue-400 transition-colors"
            type="button"
            onClick={() => {
              goTo('/live');
              onClose();
            }}>
            <HeroMapPin />
            <span className="ml-2">Go To Live View</span>
          </button>
        </li>

        <li>
          <button
            className="w-full flex hover:text-blue-600 dark:hover:text-blue-400 transition-colors"
            type="button"
            onClick={() => {
              goTo('/history');
              onClose();
            }}>
            <HeroQueueList />
            <span className="ml-2">Go To History View</span>
          </button>
        </li>

        <li>
          <button
            className="w-full flex hover:text-blue-600 dark:hover:text-blue-400 transition-colors"
            type="button"
            onClick={handleClose(resetRecords)}>
            <HeroTrash />
            <span className="ml-2">Clear Records</span>
          </button>
        </li>

        <li>
          <FileUpload
            className="flex hover:text-blue-600 dark:hover:text-blue-400 transition-colors"
            id="import-data"
            onChange={e => {
              importData(e);
              onClose();
            }}>
            <HeroArrowDownTray />
            <span className="ml-2">Import Data</span>
          </FileUpload>
        </li>

        <li>
          <button
            className="w-full flex hover:text-blue-600 dark:hover:text-blue-400 transition-colors"
            type="button"
            onClick={handleClose(exportFile)}>
            <HeroArrowUpTray />
            <span className="ml-2">Export Data</span>
          </button>
        </li>

        <li>
          <button
            className="w-full flex hover:text-blue-600 dark:hover:text-blue-400 transition-colors"
            type="button"
            onClick={handleClose(toggleTheme)}>
            {props.isDarkMode ? <HeroSun /> : <HeroMoon />}
            <span className="ml-2">{props.isDarkMode ? 'Switch Light' : 'Switch Dark'}</span>
          </button>
        </li>
      </ul>
    </div>
  );
}

Menu.propTypes = {
  onClose: func,
  toggleTheme: func,
  getCurrentPosition: func
};

export default Menu;
