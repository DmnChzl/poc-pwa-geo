import { useState } from 'react';
import CardView from '../components/CardView';
import { CARDVIEW_TYPE } from '../constants';
import { useSignalsContext } from '../SignalsContext';
import { formatTimestamp, reverseSortBy } from '../utils';

function HistoryView() {
  const [{ records, recordsLength }] = useSignalsContext();
  const [currentIdx, setCurrentIdx] = useState(0);

  return (
    <main className="flex-auto bg-white dark:bg-gray-900 overflow-y-auto scrollbar-hidden">
      <div className="p-4 flex flex-col space-y-4 md:space-y-0 md:grid md:grid-cols-2 md:gap-4">
        {records.value.sort(reverseSortBy('timestamp')).map(({ timestamp, latitude, longitude }, idx) => (
          <CardView
            key={idx}
            className="cursor-pointer"
            title={formatTimestamp(timestamp, 'PPP')}
            subTitle={formatTimestamp(timestamp, 'HH:mm:ss')}
            type={idx === currentIdx ? CARDVIEW_TYPE.INFO : CARDVIEW_TYPE.DEFAULT}
            onClick={() => setCurrentIdx(idx)}
            renderAdornment={({ className }) => (
              <span className={`shrink-0 m-2 w-8 h-8 ${className} text-[24px] text-center`}>
                {recordsLength.value - idx}
              </span>
            )}>
            {(latitude || longitude) && (
              <div className="flex">
                {latitude && <span className="w-full truncate">Lat: {latitude}</span>}
                {longitude && <span className="w-full truncate">Lon: {longitude}</span>}
              </div>
            )}
          </CardView>
        ))}
      </div>
    </main>
  );
}

export default HistoryView;
