import { number, string } from 'prop-types';
import { useNavigate } from 'react-router-dom';
import CardView from '../components/CardView';
import { HeroExclamationTriangle, HeroMap, HeroMapPin, HeroPause } from '../components/icons';
import { CARDVIEW_TYPE } from '../constants';
import { useSignalsContext } from '../SignalsContext';
import { formatTimestamp } from '../utils';

function LiveView({ errorMessage, distinctRecordsLength }) {
  const [{ lastRecord, isListening }] = useSignalsContext();
  const navigate = useNavigate();

  const renderLastPing = timestamp => {
    if (timestamp) {
      return (
        <span className="w-full">
          Last Ping: {formatTimestamp(timestamp, 'dd/MM/yyyy')} At: {formatTimestamp(timestamp, 'HH:mm:ss')}
        </span>
      );
    }

    return null;
  };

  return (
    <main className="flex-auto bg-white dark:bg-gray-900 overflow-y-auto scrollbar-hidden">
      <div className="p-4 flex flex-col space-y-4 md:space-y-0 md:grid md:grid-cols-2 md:gap-4">
        {errorMessage && (
          <CardView
            title="Error"
            subTitle={errorMessage}
            type={CARDVIEW_TYPE.DANGER}
            renderAdornment={() => (
              <HeroExclamationTriangle className="shrink-0 m-2 text-red-200" width={32} height={32} />
            )}>
            {renderLastPing(lastRecord.value?.timestamp)}
          </CardView>
        )}

        {!isListening.value && (
          <CardView
            title="Pause"
            subTitle="Application Not Automatically Listening"
            type={CARDVIEW_TYPE.WARNING}
            renderAdornment={() => <HeroPause className="shrink-0 m-2 text-yellow-200" width={32} height={32} />}
          />
        )}

        <CardView
          title={formatTimestamp(lastRecord.value?.timestamp, 'PPP')}
          subTitle={formatTimestamp(lastRecord.value?.timestamp, 'HH:mm:ss')}
          type={lastRecord.value?.timestamp ? CARDVIEW_TYPE.INFO : CARDVIEW_TYPE.DEFAULT}
          renderAdornment={({ className }) => (
            <HeroMapPin className={`shrink-0 m-2 ${className}`} width={32} height={32} />
          )}>
          {(lastRecord.value?.latitude || lastRecord.value?.longitude) && (
            <div className="flex">
              {lastRecord.value?.latitude && <span className="w-full truncate">Lat: {lastRecord.value?.latitude}</span>}
              {lastRecord.value?.longitude && (
                <span className="w-full truncate">Lon: {lastRecord.value?.longitude}</span>
              )}
            </div>
          )}
        </CardView>

        <CardView
          className="cursor-pointer"
          title="Google Maps"
          subTitle="Show Pins On The Map"
          type={CARDVIEW_TYPE.SUCCESS}
          renderAdornment={({ className }) => (
            <HeroMap className={`shrink-0 m-2 ${className}`} width={32} height={32} />
          )}
          onClick={() => navigate('/map')}>
          <span className="w-full">
            There{' '}
            {distinctRecordsLength > 1
              ? `Are ${distinctRecordsLength} Distinct Records`
              : `Is ${distinctRecordsLength} Record`}
          </span>
        </CardView>
      </div>
    </main>
  );
}

LiveView.propTypes = {
  errorMessage: string,
  distinctRecordsLength: number
};

export default LiveView;
