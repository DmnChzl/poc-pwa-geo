import { useEffect, useState } from 'react';
import { HeroEllipsisVertical } from '../components/icons';
import { COLOR, ITEM_KEY, THEME } from '../constants';
import * as StorageService from '../services/storageService';
import Menu from './Menu';

const setMetaColor = color => {
  const themeColor = document.querySelector('meta[name=theme-color]');
  themeColor.setAttribute('content', color);
};

export default function TopBar(props) {
  const [theme, setTheme] = useState(THEME.LIGHT);
  const [showMenu, setShowMenu] = useState(false);

  const switchDark = () => {
    document.documentElement.classList.add(THEME.DARK);
    setMetaColor(COLOR.GRAY_800);
    setTheme(THEME.DARK);
  };

  const switchLight = () => {
    document.documentElement.classList.remove(THEME.DARK);
    setMetaColor(COLOR.GRAY_100);
    setTheme(THEME.LIGHT);
  };

  useEffect(() => {
    StorageService.getItem(ITEM_KEY, true).then(item => {
      if (item?.theme === THEME.DARK) switchDark();
    });
  }, []);

  useEffect(() => {
    StorageService.patchItem(ITEM_KEY, { theme }, true);
  }, [theme]);

  const toggleTheme = () => {
    if (document.documentElement.classList.contains(THEME.DARK)) {
      switchLight();
    } else {
      switchDark();
    }
  };

  return (
    <header className="flex items-center flex-grow-0 flex-shrink p-3 bg-gray-100 dark:bg-gray-800 text-gray-800 dark:text-gray-100 z-10 shadow">
      <h1 className="font-semibold tracking-wide">PIN(G)</h1>
      <button className="ml-auto" type="button" onClick={() => setShowMenu(value => !value)}>
        <HeroEllipsisVertical aria-hidden />
      </button>

      {showMenu && (
        <Menu
          onClose={() => setShowMenu(false)}
          isDarkMode={theme === THEME.DARK}
          toggleTheme={toggleTheme}
          {...props}
        />
      )}
    </header>
  );
}
