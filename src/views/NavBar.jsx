import clsx from 'clsx';
import { bool, func } from 'prop-types';
import { Link, useLocation } from 'react-router-dom';
import { HeroArrowPath, HeroMapPin, HeroQueueList } from '../components/icons';

function NavBar({ getCurrentPosition, loading, isError }) {
  const location = useLocation();
  const isActive = pathname => location.pathname === pathname;

  return (
    <footer className="relative flex flex-grow-0 flex-shrink p-3 h-18 bg-gray-100 dark:bg-gray-800 text-gray-800 dark:text-white -shadow z-10">
      <button
        className={clsx(
          'absolute top-0 left-[50%] w-12 h-12 flex text-gray-100 dark:text-gray-800 -translate-x-2/4 -translate-y-2/4 rotate-45 transition-all rounded',

          loading ? 'bg-blue-600 dark:bg-blue-400' : 'bg-gray-800 dark:bg-gray-100',
          isError && 'scale-0'
        )}
        type="button"
        onClick={getCurrentPosition}
        disabled={loading || isError}>
        <HeroArrowPath className={clsx('m-auto -rotate-45', loading && 'animate-rotate')} />
      </button>

      <Link
        to="/live"
        className={clsx(
          'mx-auto flex items-center text-gray-800 dark:text-gray-100 hover:text-blue-600 dark:hover:text-blue-400 transition-colors',
          isActive('/live') && 'text-blue-600 dark:text-blue-400'
        )}>
        <HeroMapPin />
        <span className="ml-2 tracking-wide">Live</span>
      </Link>

      <Link
        to="/history"
        className={clsx(
          'mx-auto flex items-center text-gray-800 dark:text-gray-100 hover:text-blue-600 dark:hover:text-blue-400 transition-colors',
          isActive('/history') && 'text-blue-600 dark:text-blue-400'
        )}>
        <HeroQueueList />
        <span className="ml-2 tracking-wide">History</span>
      </Link>
    </footer>
  );
}

NavBar.propTypes = {
  getCurrentPosition: func,
  loading: bool,
  isError: bool
};

export default NavBar;
