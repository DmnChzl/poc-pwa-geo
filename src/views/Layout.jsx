import { Outlet as Slot } from 'react-router-dom';
import NavBar from './NavBar';
import TopBar from './TopBar';

export default function Layout(props) {
  return (
    <div className="flex flex-col h-screen">
      <TopBar {...props} />
      <Slot />
      <NavBar {...props} />
    </div>
  );
}
