import { beforeEach, describe, expect, it, vi } from 'vitest';
import * as StorageService from '../storageService';

const storageMock = (() => {
  let store = {};

  return {
    getItem(key) {
      return store[key] || null;
    },
    setItem(key, value) {
      store[key] = value.toString();
    },
    removeItem(key) {
      delete store[key];
    },
    clear() {
      store = {};
    }
  };
})();

Object.defineProperty(window, 'sessionStorage', {
  value: storageMock
});

Object.defineProperty(window, 'localStorage', {
  value: storageMock
});

describe('StorageService', () => {
  beforeEach(() => {
    vi.restoreAllMocks();
  });

  it('getItem', async () => {
    const getItemSpy = vi.spyOn(window.sessionStorage, 'getItem');
    window.sessionStorage.setItem('hello_world', JSON.stringify({ hello: 'world' }));
    const item = await StorageService.getItem('hello_world');

    expect(item).toEqual({ hello: 'world' });
    expect(getItemSpy).toHaveBeenCalledWith('hello_world');
  });

  it('putItem', async () => {
    const setItemSpy = vi.spyOn(window.sessionStorage, 'setItem');
    await StorageService.putItem('hello_world', { hello: 'world' });

    expect(setItemSpy).toHaveBeenCalledWith('hello_world', JSON.stringify({ hello: 'world' }));
    const item = window.sessionStorage.getItem('hello_world');
    expect(JSON.parse(item)).toEqual({ hello: 'world' });
  });

  it('patchItem', async () => {
    const setItemSpy = vi.spyOn(window.sessionStorage, 'setItem');
    await StorageService.patchItem('hello_world_lorem_ipsum', { hello: 'world' });
    await StorageService.patchItem('hello_world_lorem_ipsum', { lorem: 'ipsum' });

    // expect(setItemSpy).toHaveBeenCalledWith('hello_world_lorem_ipsum', JSON.stringify({ hello: 'world' }));
    // expect(setItemSpy).toHaveBeenCalledWith('hello_world_lorem_ipsum', JSON.stringify({ lorem: 'ipsum' }));
    expect(setItemSpy).toHaveBeenCalledTimes(2);
    const item = window.sessionStorage.getItem('hello_world_lorem_ipsum');
    expect(JSON.parse(item)).toEqual({ hello: 'world', lorem: 'ipsum' });
  });
});
