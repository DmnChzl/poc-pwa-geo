import { describe, expect, it, vi } from 'vitest';
import * as LocationService from '../locationService';

describe('StorageService', () => {
  const mockGeolocation = {
    getCurrentPosition: vi.fn().mockImplementationOnce(success =>
      Promise.resolve(
        success({
          coords: {
            latitude: 46.3238,
            longitude: -0.4654
          }
        })
      )
    )
  };

  it('getCurrentPosition', async () => {
    global.navigator.geolocation = mockGeolocation;

    const position = await LocationService.getCurrentPosition();
    expect(position.coords).toEqual({
      latitude: 46.3238,
      longitude: -0.4654
    });
  });
});
