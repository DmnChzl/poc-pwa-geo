export const getItem = (key, useLocal = false) => {
  const storage = useLocal ? localStorage : sessionStorage;

  return new Promise((resolve, reject) => {
    try {
      const item = storage.getItem(key);
      const value = JSON.parse(item);

      resolve(value);
    } catch (err) {
      reject(err);
    }
  });
};

export const putItem = (key, value, useLocal = false) => {
  const storage = useLocal ? localStorage : sessionStorage;

  return new Promise((resolve, reject) => {
    try {
      const json = JSON.stringify(value);

      storage.setItem(key, json);
      resolve(value);
    } catch (err) {
      reject(err);
    }
  });
};

export const patchItem = (key, value, useLocal = false) => {
  const storage = useLocal ? localStorage : sessionStorage;

  return new Promise((resolve, reject) => {
    try {
      const item = storage.getItem(key);
      const values = { ...JSON.parse(item), ...value };
      const json = JSON.stringify(values);

      storage.setItem(key, json);
      resolve(value);
    } catch (err) {
      reject(err);
    }
  });
};
