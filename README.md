[![Netlify Status](https://api.netlify.com/api/v1/badges/fd1eff16-d578-4659-b219-e4572282dc83/deploy-status)](https://app.netlify.com/sites/pin-poc/deploys)

# Pin(G)

Technical project which consists in using into the browser's API, in order to retrieve geolocation data (latitude / longitude), in **PWA\*** mode (**\*P**rogressive **W**eb **A**pp)

## Business Problem

> Is it possible to retrieve geolocation data, when the device is offline (in the background), at a regular frequency?

## Technical Environment

- **React** ([CRA](https://create-react-app.dev))
- [Workbox](https://developer.chrome.com/docs/workbox) (Service Worker)
- [Preact Signals](https://preactjs.com/guide/v10/signals)
- [TailwindCSS](https://tailwindcss.com)
- [Playwright](https://playwright.dev)

## Project Structure

```
/
├── e2e/
├── public/
│   └── index.html
├── src/
│   ├── components/
│   ├── constants/
│   ├── hooks/
│   │   └── useInterval.js
│   ├── services/
│   │   ├── locationService.js
│   │   └── storageService.js
│   ├── signals/
│   ├── utils/
│   ├── views/
│   ├── index.css
│   ├── index.js
│   └── service-worker.js
├── package.json
├── README.md
└── tailwind.config.js
```

## Process

Clone the project:

```
git clone https://gitlab.com/dmnchzl/ping.git
```

Install dependencies:

```
npm install
```

Develop locally:

```
npm run start
```

Compile the project:

```
npm run build
```

Preview the project:

```
npx serve -s build
```

## Reporting

### Use Cases

- Launching the application for 5 minutes; after 300 seconds, the app added 30 records with coordinates (latitude/longitude)
- Launch of the application in "_offline_" mode for 2 minutes; after 120 seconds, when the network is back, the app added 12 records with same coordinates (latitude / longitude) (corresponding to when the network came back)

### Opportunity(ies) / Limit(s)

**Opportunity(ies)**:

- Data fetching, every "_x_" seconds (frequency\*)
- Retrieving data manually (`<button>`)
- Access to the application (and to the data history) in "_offline_" mode
- Saving data in (_session_ / _local_) _storage_ of the browser
- Recordings export / import

> \* 10 second delay (i.e. `<App />` / `useInterval()`)

**Limit(s)**:

- Blocking the geolocation API in "_offline_" mode \*\*

> \*\* The application calls the browser's API, the environment adds a "_callback_" in the JavaScript _event loop_, but never returns (infinite loading)...

### Note(s):

Geolocation data recovery request via **Service Worker** is strong, and officially dates from September 5, 2015 (see sources). There is currently no solution to retrieve coordinates through the Web (in "_offline_" mode), only by consuming the network / phone data.

[W3C](https://www.w3.org) Proposal(s):

- ~~TravelManager API~~
- ~~GeoFencing API~~

Considered Solution:

> Using phone gyroscope natively (Android - Kotlin / iOS - Swift / Hybrid - Cordova!?)

## Source(s)

- [GeoLocation API](https://developer.mozilla.org/fr-FR/docs/Web/API/Geolocation_API)
- [Expose GeoLocation To Workers](https://github.com/w3c/ServiceWorker/issues/745)
- [StackOverFlow](https://bit.ly/3UhsLqX)