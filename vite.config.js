import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { VitePWA } from 'vite-plugin-pwa';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    VitePWA({
      includeAssets: [
        'favicon48.png',
        'favicon72.png',
        'favicon96.png',
        'favicon128.png',
        'favicon144.png',
        'favicon192.png',
        'favicon384.png',
        'favicon512.png'
      ],
      manifest: {
        name: 'Pin(G)',
        short_name: 'Pin(G)',
        description: 'Proof Of Concept - Geo x PWA',
        theme_color: '#f3f4f6',
        background_color: '#fff',
        icons: [
          {
            src: 'favicon48.png',
            type: 'image/png',
            sizes: '48x48'
          },
          {
            src: 'favicon72.png',
            type: 'image/png',
            sizes: '72x72'
          },
          {
            src: 'favicon96.png',
            type: 'image/png',
            sizes: '96x96'
          },
          {
            src: 'favicon128.png',
            type: 'image/png',
            sizes: '128x128'
          },
          {
            src: 'favicon144.png',
            type: 'image/png',
            sizes: '144x144'
          },
          {
            src: 'favicon192.png',
            type: 'image/png',
            sizes: '192x192'
          },
          {
            src: 'favicon384.png',
            type: 'image/png',
            sizes: '384x384'
          },
          {
            src: 'favicon512.png',
            type: 'image/png',
            sizes: '512x512',
            purpose: 'maskable'
          }
        ],
        start_url: '.',
        display: 'standalone'
      }
    })
  ],
  preview: {
    port: 2468
  },
  server: {
    port: 1234
  },
  test: {
    environment: 'jsdom',
    setupFiles: './src/setupTests.js'
  }
});
