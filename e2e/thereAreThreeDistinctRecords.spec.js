import { expect, test } from '@playwright/test';

test.use({
  geolocation: {
    latitude: 46.3238,
    longitude: -0.4654
  },
  permissions: ['geolocation'],
  storageState: 'e2e/__mocks__/storage.json',
  timezoneId: 'Europe/Paris',
  viewport: {
    height: 600,
    width: 800
  }
});

test('There Are 3 Distinct Records', async ({ page }) => {
  await page.goto('http://localhost:2468/');
  await page.goto('http://localhost:2468/live');

  await page.getByRole('banner').getByRole('button').click();
  await page.getByRole('button', { name: 'Pause' }).click();
  await page.getByRole('link', { name: 'History' }).click();

  await expect(page.getByText('October 3rd, 2022')).toBeVisible();
  await expect(page.getByText('08:15:00')).toBeVisible();
  await expect(page.getByText('Lat: 48.0048')).toBeVisible();
  await expect(page.getByText('Lon: 0.2015')).toBeVisible();

  await expect(page.getByText('November 2nd, 2022')).toBeVisible();
  await expect(page.getByText('12:30:00')).toBeVisible();
  await expect(page.getByText('Lat: 46.3238')).toBeVisible();
  await expect(page.getByText('Lon: -0.4637')).toBeVisible();

  await expect(page.getByText('December 1st, 2022')).toBeVisible();
  await expect(page.getByText('16:45:00')).toBeVisible();
  await expect(page.getByText('Lat: 46.1607')).toBeVisible();
  await expect(page.getByText('Lon: -1.1541')).toBeVisible();

  await page.getByRole('banner').getByRole('button').click();
  await page.getByRole('button', { name: 'Go To Live View' }).click();

  await expect(page.getByText('There Are 3 Distinct Records')).toBeVisible();
});
